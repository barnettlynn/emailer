package emailer

import (
	"net/smtp"
)

type emailer struct {
	username string
	password string
	host     string
	port     string
	auth     smtp.Auth
}

func GetEmailer(username string, password string, host string, port string) emailer {

	newEmailer := emailer{}

	newEmailer.username = username
	newEmailer.password = password
	newEmailer.host = host
	newEmailer.port = port

	newEmailer.auth = smtp.PlainAuth("", newEmailer.username, newEmailer.password, newEmailer.host)

	return newEmailer
}

func (e *emailer) SendEmail(to string, subject string, msg string) error {

	outGoingMessage := "From: " + e.username + "\n" + "To: " + to + "\n" + "Subject: " + subject + "\n\n" + msg

	err := smtp.SendMail(e.getHostWithPort(), e.auth, e.username, []string{to}, []byte(outGoingMessage))
	if err != nil {
		return err
	}
	return nil
}

func (e *emailer) getHostWithPort() string {
	return e.host + ":" + e.port
}
